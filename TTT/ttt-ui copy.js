$(document).ready(function () {

    var game = new window.TTT.Game();

    $.each(game.board, function (rowI, value) {

        var row = $('<div class="row" id=row"'+ rowI +'"></div>');
        $('body').append(row);

        $.each(value, function (colI, value) {
            var square = $('<div class="square" data-col="'+colI+'" data-row="'+rowI+'"></div>');
            row.append(square);
        });
    });

    $('.square').on("click", function (event) {
        var row = $(this).data("row");
        var col = $(this).data("col");

        if (game.move([row, col])) {
            game.player === "x" ? $(this).addClass("blue") : $(this).addClass("red");
            if (game.winner()) {
                $('#info').addClass("success").html("WINNER!");
            }
        } else {
            $('#info').addClass("error").html("INVALID!").fadeOut("slow", function () {
                $('#info').removeClass("error").html("Tic Tac Toe").fadeIn("slow");
            });

        }


      $(this).addClass("blue");
    });



});