$(document).ready(function () {

    window.game = new window.Hanoi.Game();

    var draw = function () {

        $("#gameWrapper").empty();

        $.each(game.towers, function (colI, value) {

            var col = $('<div class="col" data-col="'+ colI +'"></div>');
            $('#gameWrapper').append(col);

            $.each(value, function (rowI, value) {
                var size;
                if ( value == 3) {
                    size = "big";
                } else if (value == 2) {
                    size = "medium";
                } else {
                    size = "small";
                }
                var disc = $('<div class="disc '+ size +'"></div>');
                col.prepend(disc);
            });
        });

        var towerFrom = null;
        var towerTo = null;

        $('.col').on("click", function (event) {
            console.log(game)

            if (towerFrom === null) {
                towerFrom = $(this).data("col");
                $(this).addClass("from")
            } else {
                towerTo = $(this).data("col");
                game.move(towerFrom, towerTo);
                towerFrom = null;
                towerTo = null;
                draw();

                if (game.isWon()) {
                    $('#info').html("WINNER!")
                }
            }
        });
    }

    draw();

});