(function (root) {

  var SnakeGame = root.SnakeGame = (root.SnakeGame || {});

  var Snake = SnakeGame.Snake = function (startPos, dir, color) {
      this.color = color;
      this.dir = dir;
      var startRow = startPos[0];
      var startCol = startPos[1];
      this.segments = [[startRow, startCol]];
      this.grow = false;
      this.directionChange = false;
  }

  _.extend(Snake.prototype, {
      move: function () {

          var headRow = this.segments[0][0];
          var headCol = this.segments[0][1];

          if (!this.grow) {
              this.segments.pop();
          } else {
              this.grow = false;
          }

          var nextCol;
          var nextRow;
          if (this.dir === "E") {
              nextCol = headCol + 1;
              this.segments.unshift([headRow, nextCol]);
          } else if (this.dir === "W") {
              nextCol = headCol - 1;
              this.segments.unshift([headRow, nextCol]);
          } else if (this.dir === "N") {
              nextRow = headRow - 1;
              this.segments.unshift([nextRow, headCol]);
          } else {
              nextRow = headRow + 1;
              this.segments.unshift([nextRow, headCol]);
          }

          this.directionChange = false;
      },

  });

})(window);

