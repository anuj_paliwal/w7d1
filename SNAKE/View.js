(function (root) {

  var SnakeGame = root.SnakeGame = (root.SnakeGame || {});

  var View = SnakeGame.View = function (wrapperElement) {
      this.$el = $(wrapperElement);
      this.board = new window.SnakeGame.Board();
  }

  _.extend(View.prototype, {
      draw: function () {
          this.$el.empty();
          var self = this;
          _.times(25, function (row) {
              var rowEl = $('<div class="row"></div>');
              self.$el.append(rowEl);
              _.times(25, function (col) {
                  var space = self.board.grid[row][col];
                  if (space === "S1") {
                      rowEl.append('<div class="col snake1"></div>');
                  } else if (space === "A"){
                      rowEl.append('<div class="col apple"></div>');
                  } else if (space == "S2") {
                      rowEl.append('<div class="col snake2"></div>');
                  } else {
                      rowEl.append('<div class="col"></div>');
                  }
              });
          });
      },
      start: function () {
          var self = this;

          $(document).keydown(function (event) {
              var keyCode = event.which;
              if (keyCode > 64) {
                  self.handleEvent(self.board.snake1, keyCode, [65,87,68,83])
              } else {
                  self.handleEvent(self.board.snake2, keyCode, [37, 38, 39, 40])
              }
          });

         this.interval = setInterval(function () {
              self.draw();
              self.board.turn();
              if (self.board.gameOver) {
                  clearInterval(self.interval);
                  $("#info").html("GAME OVER! " + self.board.losingSnake + " Loses!");
              }
              self.directionChange = false;
          }, 250);
      },
      handleEvent: function (snake, code, codes) {
          var snakeDir = snake.dir;

          if (snake.directionChange) { return; }
          if (code == codes[0]) {
              if (snakeDir != "E") {
                  snake.dir = "W";
                  snake.directionChange = true;
              }
          } else if (code == codes[1]) {
              if (snakeDir != "S") {
                  snake.dir = "N";
                  snake.directionChange = true;
              }
          } else if (code == codes[2]) {
              if (snakeDir != "W") {
                  snake.dir = "E";
                  snake.directionChange = true;
              }
          } else if (code == codes[3]) {
              if (snakeDir != "N") {
                  snake.dir = "S";
                  snake.directionChange = true;
              }
          }
      }
  });

})(window);

var game = new window.SnakeGame.View($('#wrapper'));
game.start();