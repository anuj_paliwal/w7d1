(function (root) {

  var SnakeGame = root.SnakeGame = (root.SnakeGame || {});

  var Board = SnakeGame.Board = function () {
      this.snake1 = new window.SnakeGame.Snake([5,5], "E", "Blue");
      this.snake2 = new window.SnakeGame.Snake([20,20], 'W', "Yellow");
      this.apples = [];
      this.addRandomApple();
      this.updateGrid();
      this.gameOver = false;
      this.losingSnake = null;
  }

  _.extend(Board.prototype, {
      updateGrid: function () {
          var grid = [];
          _.times(25, function (row) {
              grid.push([]);
              _.times(25, function (col) {
                  grid[row].push(null);
              })
          });
          this.grid = grid;
          this.addToGrid(this.snake1.segments, "S1");
          this.addToGrid(this.snake2.segments, "S2");
          this.addToGrid(this.apples, "A");
      },
      addToGrid: function (array, sym) {
          var self = this;
          _.each(array, function (pos) {
              self.grid[pos[0]][pos[1]] = sym;
          });
      },
      moveSnake: function (snake) {
          snake.move();
          var headPos = snake.segments[0];

          if (headPos[0] > 24 || headPos[1] > 24 || headPos[0] < 0 || headPos[1] < 0) {
              console.log("OUT OF BOUNDS");
              this.gameOver = true;
              this.losingSnake = snake.color;
              return;
          } else {
              var spaceAtHead = this.grid[headPos[0]][headPos[1]];
          }
          if (spaceAtHead === "A") {
              snake.grow = true;
              this.apples.pop();
              this.addRandomApple();
          } else if (spaceAtHead === "S1" || spaceAtHead === "S2") {
              this.gameOver = true;
              this.losingSnake = snake.color;
          }
          this.updateGrid();
      },
      turn: function () {
          this.moveSnake(this.snake1);
          this.moveSnake(this.snake2);
      },
      setSnakeDir: function (dir) {
          this.snake.dir = dir;
      },
      addRandomApple: function () {
          var randRow = this.randRange(0, 24);
          var randCol = this.randRange(0, 24);
          if (this.snake1.segments.indexOf([randRow, randCol]) == -1 || this.snake2.segments.indexOf([randRow, randCol]) == -1) {
              this.apples.push([randRow, randCol]);
          } else {
              console.log("APPPPLESSS");
              this.addRandomApple();
          }
      },
      randRange: function (from, to) {
          return Math.floor(Math.random()*(to-from+1)+from);
      }

  });

})(window);

